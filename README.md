CLI exercises
-----------------------------

1) Using mkdir to create directory and touch to create files.
2) using "find . -name *.txt >exercise-2/locations.txt " to print location and paste it to a .txt file.
3) using "find . -name *.log -delete" to delete all the log files  
4) using nano editor.
5) use "zip -r hello.zip hello"
6) copy the files using "cp -r" command
7) delete the files using "rm -rf <folder-name>


Harry potter exercise
-----------------------------
1) "curl https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/    Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt >harry-potter.txt "

2) "head -n 3 harry-potter.txt>first-three-lines.txt" to print 3 lines from the top.

3) "tail -n 10 harry-potter.txt>last-ten-lines.txt" to print bottom 10 lines .

4) "grep -o "<word>" ../harry-potter.txt| wc -l >count-<word>.txt" to search and write it in a particular count file.

5)"sed -n 100,200p harry-potter.txt" to print 100 to 200 lines from a txt file.
6) " cut -d ' ' -f 1 harry-potter.txt| sort |uniq -c|wc -l>count-unique-words.txt" to find unique words


Processes
-------------------------------
1) "ps axo pid,ppid,comm |grep firefox"   to get pid ppid of browser
2) "pkill chrome" or "kill <PID>" to kill a process.
3) "ps axo pcpu comm|sort -n|tail -3"  to get top 3 cpu consuming process.

Misc
--------------------------------
1) "curl ifconfig.me" to find local ip
2) "ping google.com" to find ip address of google.com
3) "whereis python" to find python path


